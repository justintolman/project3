//
//  PinFactory.m
//  Project1
//
//  Created by Justin Tolman on 4/30/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "PinFactory.h"
#import "ZSPinAnnotation.h"
#import <CoreData/CoreData.h>

@implementation PinFactory{
    MapViewController *_mapViewController;
    NSMutableArray *_remoteArray;
}
-(id)init{
    self=[super init];
    _pinArray=[[NSMutableArray alloc] init];
    return self;
}

+ (PinFactory*)sharedInstance
{
    // 1
    static PinFactory *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[PinFactory alloc] init];
    });
    return _sharedInstance;
}

-(void)setMapViewController:(MapViewController*)mvc{
    _mapViewController=mvc;
}

-(CLLocationCoordinate2D)getCoordinates{
    return _mapViewController.getCoordinates;
}

-(void) netPinsJson:(NSString *)urlString{
    MKMapView *map=_mapViewController.projectMapView;
    NSMutableArray *removeArray=[[NSMutableArray alloc] init];
    for (ZSAnnotation *pin in _pinArray) {
        if(!pin.userEntry)[removeArray addObject:pin];
    }
    [_pinArray removeObjectsInArray:removeArray];
    [map removeAnnotations:removeArray];
    removeArray=nil;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        for (NSDictionary *pin in responseObject) {
            [_pinArray addObject:[self makePin:pin[@"name"] latitude:[pin[@"location"][@"latitude"] doubleValue] longitude:[pin[@"location"][@"longitude"] doubleValue] color:[UIColor blueColor] desc:pin[@"description"] type:0 user:NO image:nil]];
        }
        [self displayPins];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"JSON failed to load. Entering cache mode." message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        if ([_pinArray count]==0) {
            [self loadCache];
        }
    }];
    [operation start];
}

-(void)addPin:(NSString *)title latitude:(double)latitude longitude:(double)longitude color:(UIColor *)color desc:(NSString *)description type:(NSUInteger)typeNumber user:(BOOL)userEntry image:(UIImage*)img
{
	[_pinArray addObject:[self makePin:title latitude:latitude longitude:longitude color:color desc:description type:typeNumber user:userEntry image:img]];
    [self displayPins];
}

-(ZSAnnotation *)makePin:(NSString *)title latitude:(double)latitude longitude:(double)longitude color:(UIColor *)color desc:(NSString *)description type:(NSUInteger) typeNumber user:(BOOL)userEntry image:(UIImage*)img {
    ZSAnnotation *pin = [[ZSAnnotation alloc] init];
	pin.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
	pin.color = color;
	pin.title = title;
	pin.subtitle = description;
    pin.userEntry=userEntry;
	pin.image = img;
    ZSPinAnnotationType annotationType;
    switch (typeNumber) {
        case 1:
            annotationType=ZSPinAnnotationTypeTag;
            break;
        case 2:
            annotationType=ZSPinAnnotationTypeDisc;
            break;
            
        default:
            annotationType=ZSPinAnnotationTypeStandard;
            break;
    }
    pin.type=annotationType;
    return pin;
}

-(void)removePin:(NSUInteger)index{
    MKMapView *map=_mapViewController.projectMapView;
    [map removeAnnotation:[_pinArray objectAtIndex:index]];
	[_pinArray removeObjectAtIndex:index];
    [self displayPins];
}

-(void)displayPins{
    if([_pinArray count]==0){
        UIAlertView *noPins = [[UIAlertView alloc] initWithTitle:@"There are no pins to display." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [noPins show];
    }else{
        MKMapView *map=_mapViewController.projectMapView;
        map.visibleMapRect = [_mapViewController makeMapRectWithAnnotations:_pinArray];
        [map addAnnotations:_pinArray];
        [self updateCache];
    }
}

-(void)updateCache{
    [self clearCache];
    NSManagedObjectContext *context = [self managedObjectContext];
    
    for (ZSAnnotation *pin in _pinArray) {
    
    // Create a new managed object
        NSManagedObject *newPin = [NSEntityDescription insertNewObjectForEntityForName:@"Pin" inManagedObjectContext:context];
        [newPin setValue:pin.title forKey:@"title"];
        [newPin setValue:pin.subtitle forKeyPath:@"subtitle"];
        [newPin setValue:[NSNumber numberWithDouble:pin.coordinate.latitude] forKeyPath:@"latitude"];
        [newPin setValue:[NSNumber numberWithDouble:pin.coordinate.longitude] forKeyPath:@"longitude"];
        int colorCode;
        if(pin.color==[UIColor redColor])colorCode=0;
        else if(pin.color==[UIColor orangeColor])colorCode=1;
        else if(pin.color==[UIColor yellowColor])colorCode=2;
        else if(pin.color==[UIColor greenColor])colorCode=3;
        else if(pin.color==[UIColor blueColor])colorCode=4;
        else if(pin.color==[UIColor purpleColor])colorCode=5;
        else if(pin.color==[UIColor blackColor])colorCode=6;
        else if(pin.color==[UIColor whiteColor])colorCode=7;
    
        [newPin setValue:[NSNumber numberWithInt:colorCode] forKeyPath:@"color"];
        int typeIndex;
        if(pin.type==ZSPinAnnotationTypeStandard)typeIndex=0;
        else if(pin.type==ZSPinAnnotationTypeTag)typeIndex=1;
        else if(pin.type==ZSPinAnnotationTypeDisc)typeIndex=2;
        else typeIndex=0;
        [newPin setValue:[NSNumber numberWithInt:typeIndex] forKeyPath:@"type"];
        [newPin setValue:UIImagePNGRepresentation(pin.image) forKeyPath:@"image"];
        [newPin setValue:[NSNumber numberWithBool:pin.userEntry] forKeyPath:@"userEntry"];

    
        NSError *error = nil;
    // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
    }
}

-(void)loadCache{
    // Fetch the devices from persistent data store
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Pin"];
    
    NSArray *tempPins = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *pin in tempPins) {
        [_pinArray addObject: [self makePin:[pin valueForKey:@"title"] latitude:[[pin valueForKey:@"latitude"] doubleValue] longitude:[[pin valueForKey:@"longitude"] doubleValue] color:[UIColor blueColor] desc:[pin valueForKey:@"title"] type:0 user:NO image:nil]];
    }
    [self displayPins];
}


-(void)clearCache{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Pin"];
    
    NSMutableArray *tempPins = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *pin in tempPins) {
        if(pin)[context deleteObject:pin];
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
@end
