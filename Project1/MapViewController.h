//
//  ViewController.h
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "EditViewController.h"

@interface MapViewController : UIViewController<MKMapViewDelegate>
@property (strong, nonatomic) IBOutlet MKMapView *projectMapView;
- (MKMapRect)makeMapRectWithAnnotations:(NSArray *)annotations;
-(CLLocationCoordinate2D)getCoordinates;
@end
