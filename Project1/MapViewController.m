//
//  ViewController.m
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "MapViewController.h"
#import "PinFactory.h"

@interface MapViewController (){
    PinFactory *_pinBuilder;
}
@end

@implementation MapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _projectMapView.delegate=self;
    _pinBuilder=[PinFactory sharedInstance];
    [_pinBuilder setMapViewController:self];
    [_pinBuilder netPinsJson:@"http://zstudiolabs.com/labs/compsci497/buildings.json"];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    //[self centerOnLocation];
}

- (IBAction)goToLocation:(id)sender {
    [self centerOnLocation];
}

-(void)centerOnLocation
{
    [_projectMapView setCenterCoordinate:_projectMapView.userLocation.coordinate animated:YES];
}

- (MKMapRect)makeMapRectWithAnnotations:(NSArray *)annotations {
	
	MKMapRect flyTo = MKMapRectNull;
    for (id <MKAnnotation> annotation in annotations) {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
        if (MKMapRectIsNull(flyTo)) {
            flyTo = pointRect;
        } else {
            flyTo = MKMapRectUnion(flyTo, pointRect);
        }
    }
	
	return flyTo;
	
}


- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
	
    // Don't mess with user location
	if(![annotation isKindOfClass:[ZSAnnotation class]])
        return nil;
    
    ZSAnnotation *a = (ZSAnnotation *)annotation;
    static NSString *defaultPinID = @"StandardIdentifier";
    
    // Create the ZSPinAnnotation object and reuse it
    ZSPinAnnotation *pinView = (ZSPinAnnotation *)[_projectMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil){
        pinView = [[ZSPinAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    }
    
    // Set the type of pin to draw and the color
    pinView.annotationType = a.type;
    pinView.annotationColor = a.color;
    pinView.canShowCallout = YES;
    UIImageView *imageView=[[UIImageView alloc] init];
    imageView.image=a.image;
    imageView.frame=CGRectMake(0,0,32,32);
    
    if(pinView.image!=nil)[pinView setLeftCalloutAccessoryView:imageView];
    return pinView;
	
}

- (IBAction)refreshJson:(id)sender {
    [_pinBuilder netPinsJson:@"http://zstudiolabs.com/labs/compsci497/buildings.json"];
}

-(CLLocationCoordinate2D)getCoordinates{
    return _projectMapView.userLocation.coordinate;
}


@end