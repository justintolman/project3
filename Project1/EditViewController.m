//
//  EditViewController.m
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import "EditViewController.h"
#import "PinFactory.h"

@interface EditViewController (){
    NSArray *colorArray;
    double _latitude;
    double _longitude;
    PinFactory *_pinBuilder;
    BOOL _editMode;
    NSString *_title;
    NSString *_description;
    UIImage *_image;
    ZSAnnotation *_pin;
    
}
@property (strong, nonatomic) IBOutlet UITextField *titleInput;
@property (strong, nonatomic) IBOutlet UITextField *descriptionInput;
@property (strong, nonatomic) IBOutlet UILabel *colorDisplay;
@property (strong, nonatomic) IBOutlet UIPickerView *colorPicker;
@property (strong, nonatomic) IBOutlet UILabel *latitudeDisplay;
@property (strong, nonatomic) IBOutlet UILabel *longitudeDisplay;
@property (strong, nonatomic) IBOutlet UISegmentedControl *pinSelect;
@property (strong, nonatomic) IBOutlet UIImageView *imagePreview;


@end

@implementation EditViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    colorArray=[[NSArray alloc] initWithObjects:@"Red",@"Orange",@"yellow",@"Green",@"Blue",@"Purple",@"Black",@"White", nil];
    
    _pinBuilder=[PinFactory sharedInstance];
}

- (void) viewDidAppear:(BOOL) animated {
    if(!_editMode){
    CLLocationCoordinate2D coordinates=_pinBuilder.getCoordinates;
        _latitude=coordinates.latitude;
        _longitude=coordinates.longitude;
    }
    _latitudeDisplay.text=[NSString stringWithFormat:@"%f",_latitude];
    _longitudeDisplay.text=[NSString stringWithFormat:@"%f",_longitude];
    if(_editMode){
        _titleInput.text=_title;
        _imagePreview.image=_image;
        _descriptionInput.text=_description;
    }
}

-(void)setPinData:(NSString*)title latitude:(double)lat longitude:(double)lon image:(UIImage*)img description:(NSString*)desc pin:(ZSAnnotation*)pin{
    _title=title;
    _latitude=lat;
    _longitude=lon;
    _image=img;
    _description=desc;
    _editMode=YES;
    _pin=pin;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent: (NSInteger)component
{
    return [colorArray count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [colorArray objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row   inComponent:(NSInteger)component{
    switch(row){
        case 0:
            _colorDisplay.backgroundColor=[UIColor redColor];
            break;
        case 1:
            _colorDisplay.backgroundColor=[UIColor orangeColor];
            break;
        case 2:
            _colorDisplay.backgroundColor=[UIColor yellowColor];
            break;
        case 3:
            _colorDisplay.backgroundColor=[UIColor greenColor];
            break;
        case 4:
            _colorDisplay.backgroundColor=[UIColor blueColor];
            break;
        case 5:
            _colorDisplay.backgroundColor=[UIColor purpleColor];
            break;
        case 6:
            _colorDisplay.backgroundColor=[UIColor blackColor];
            break;
        case 7:
            _colorDisplay.backgroundColor=[UIColor whiteColor];
            break;
    }
    
}

- (IBAction)createPin:(id)sender {
    if (_editMode) {
        _pin.title=_titleInput.text;
        _pin.subtitle=_descriptionInput.text;
        _pin.color=_colorDisplay.backgroundColor;
        ZSPinAnnotationType annotationType;
        switch (_pinSelect.selectedSegmentIndex) {
            case 1:
                annotationType=ZSPinAnnotationTypeTag;
                break;
            case 2:
                annotationType=ZSPinAnnotationTypeDisc;
                break;
                
            default:
                annotationType=ZSPinAnnotationTypeStandard;
                break;
        }
        _pin.type=annotationType;
        _pin.image=_imagePreview.image;

        
    }else{
        [_pinBuilder addPin:_titleInput.text latitude:_latitude longitude:_longitude color:_colorDisplay.backgroundColor desc:_descriptionInput.text type:_pinSelect.selectedSegmentIndex user:YES image:_imagePreview.image];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)openCamera:(id)sender{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"No camera available."
                                                              message:@"Select an image instead."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];

        
    }else{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;

        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _imagePreview.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
