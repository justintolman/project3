//
//  AppDelegate.h
//  Project1
//
//  Created by Justin Tolman on 4/29/14.
//  Copyright (c) 2014 Justin Tolman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLConnectionDataDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic, strong)NSMutableData *responseData;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end